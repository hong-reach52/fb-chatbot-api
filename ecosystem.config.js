module.exports = {
  "apps" : [{
    "name"      : "fb-selfsignup-api",
    "script"    : "index.js",
    "watch"     : ["./www", "./index.js"],
    "env": {
      "COMMON_ENV_VAR": "true"
    },
    "env_production": {
      "NODE_ENV": "dev",
    }
  }],
  "deploy" : {
    "production" : {
      "user" : "node",
      "host" : "0.0.0.0",
      "repo" : "https://sudzz@bitbucket.org/sudzz/fb-chatbot-api.git",
      "ref"  : "origin/master",
      "path" : "./www",
      "post-deploy" : "pm2 startOrRestart ecosystem.config.js --env production"
    },
    "dev" : {
      "user" : "node",
      "host" : "0.0.0.0",
      "repo" : "https://sudzz@bitbucket.org/sudzz/fb-chatbot-api.git",
      "ref"  : "origin/master",
      "path" : "./www",
      "post-deploy" : "pm2 startOrRestart ecosystem.config.js --env dev"
    }
  }
};