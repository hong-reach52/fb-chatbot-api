//`1Diagchat
'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const requestPromise = require('request-promise');
const url = require('url');
const restService = express();
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const ngrok = require('ngrok');

const routerValidator = require('./www/controller/validator');
const routerHealthCheckScoring = require('./www/controller/healthcheckscoring');
const routerEatingHabitScoring = require('./www/controller/eatinghabitsscoring');
const routerEatingValidator = require('./www/controller/eatingvalidator');
const { positive } = require('./www/controller/dengue');
// const routerEatingValidator = require('./www/controller/eatingvalidator');
var restUrl = "https://fbwebhook.alliedworld.healthcare";

restService.engine('html', require('ejs').renderFile);

restService.use(bodyParser.urlencoded({
    extended: true
}));

restService.use(bodyParser.json());
/*****************************************************************************************/
restService.use(routerEatingValidator);
restService.use(routerValidator);
restService.use(routerHealthCheckScoring);
restService.use(routerEatingHabitScoring);
restService.use("/hypertension", require("./www/route/hypertenstion.js"));
restService.use("/diabetes", require("./www/route/diabetes.js"));
restService.post("/dengue/positive", positive);

/*****************************************************************************************/
/*(async function() {
    const url = await ngrok.connect(8000);
    console.log(url);
})();*/
restService.listen((process.env.PORT || 8000), () => console.log('Express server is listening on port 8000'));