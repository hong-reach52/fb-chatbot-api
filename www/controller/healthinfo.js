const express = require('express');
const routerHealthInfo = require('express').Router();
const model = require("../model/datahandler");

var errorInputMessage = {
                          "Eng" : "You have input invalid choices. Please try again",
                          "Hil" : "Sala nga choices ang imo gin-enter. Palihog liwat sang imo sabat."
}

routerHealthInfo.post('/health_info', function(req, res) {
    console.log("routerHealthInfo post");
    var lang = req.body.lang.substring(0,3);
    var allergy = req.body.allergy;
    var disease_history = req.body.disease_history;
    var jsonResponse = {};
    
    if(typeof allergy !== "undefined") {
        if(allergy.split(",").some(isNaN) &&
           allergy.toLowerCase() != "none" &&
           allergy.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Health Info 3-0"]
			};
        } else {
            jsonResponse = {
                                //"redirect_to_blocks": [lang + " Health Info 4"]
			};
        }
    }

    if(typeof disease_history !== "undefined") {
        if(disease_history.split(",").some(isNaN) &&
           disease_history.toLowerCase() != "none" &&
           disease_history.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Health Info 4"]
			};
        } else {
            /*jsonResponse = {
                                "redirect_to_blocks": [lang + " Health Info 4"]
			};*/
        }
    }
    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerHealthInfo.post('/save_health_info', function(req, res) {
    console.log("routerHealthInfo post");   
    var jsonResponse = {};

    model.save_data("health_info", req.body);

    var jsonResponse = {
 			"messages": [
   					{"text": "Your health info data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});


module.exports = routerHealthInfo;