const express = require('express');
const routerEatingHabitScoring = require('express').Router();
var moment = require('moment');

var eatingCodes = require('../model/eatinghabits.json');
var bmi = require('../compute/bmi.js');
const { lang } = require('moment');

// interpret user healthy eating answers
// Author: Lorlyn Toriaga
// July 02, 2020
routerEatingHabitScoring.post('/get_eatingscore', function(req, res) {
  console.log(req.body)
  // map user request from chatbot to JSON
  let healthyEatingJSON = {
    attribute : req.body.attribute,
    user: {
        weight:               req.body.user.weight,
        height:               req.body.user.height,
        cupsizeFruitsandVeg:  req.body.user.cupsizeFruitsandVeg,
        friedAndFats:         req.body.user.friedAndFats,
        saltyAndSweets:       req.body.user.saltyAndSweets,
        plateContain:         req.body.user.plateContain,
        sweetenDrinks:        req.body.user.sweetenDrinks
    },
    messageType: req.body.messageType,
    lang: req.body.lang.substring(0,3)
  }

  let expectingResults = ['0', '1-3', '2-3.meals', 'once.a.day', 
                          '1', '2', '2-3.times'];

  let goodResult = ['6.or.more','occasionally','all', '4-6','weekly','3'];

  let bmiResult = bmi.compute(healthyEatingJSON.user.height, healthyEatingJSON.user.weight);

  let getCodeBMI = eatingCodes[healthyEatingJSON.lang].bmi.find(element => element.code == bmiResult.code)
  let getCodeCupsize = eatingCodes[healthyEatingJSON.lang].cupsize.find(element => element.text == healthyEatingJSON.user.cupsizeFruitsandVeg)
  let getCodeFriedAndFats = eatingCodes[healthyEatingJSON.lang].friedAndFats.find(element => element.text == healthyEatingJSON.user.friedAndFats)  
  let getCodeSaltyAndSweet = eatingCodes[healthyEatingJSON.lang].saltyAndSweets.find(element => element.text == healthyEatingJSON.user.saltyAndSweets)
  let getCodePlateContain = eatingCodes[healthyEatingJSON.lang].plateContain.find(element => element.text == healthyEatingJSON.user.plateContain)
  let getCodeSweetenDrinks = eatingCodes[healthyEatingJSON.lang].drinks.find(element => element.text == healthyEatingJSON.user.sweetenDrinks)

  let returningBlocks = {
    redirect_to_blocks: ["eating.bmi.results.breakdown1." + healthyEatingJSON.lang],
    set_attributes: {
      "eating.bmi.result.block" : getCodeBMI.code,
      "user.healthy.eating.bmi": bmiResult.result,
      "v.n.f.results.block": getCodeCupsize.code,
      "f.n.f.result.block": getCodeFriedAndFats.code,
      "s.n.s.result.block": getCodeSaltyAndSweet.code,
      "plate.contain.result.block": getCodePlateContain.code,
      "drinks.result.block": getCodeSweetenDrinks.code
    }
  };
  
  if (((expectingResults.indexOf(getCodeCupsize.code) != -1) && (expectingResults.indexOf(getCodeFriedAndFats.code) != -1)
  && (expectingResults.indexOf(getCodeSaltyAndSweet.code) != -1) && (expectingResults.indexOf(getCodePlateContain.code) != -1)
  && (expectingResults.indexOf(getCodeSweetenDrinks.code) != -1))){
    returningBlocks.set_attributes["gallery.for.interpretations"] = "true";
  } else{
    returningBlocks.set_attributes["gallery.for.interpretations"] = "false";
  }
  
  if (((goodResult.indexOf(getCodeCupsize.code) != -1) && (goodResult.indexOf(getCodeFriedAndFats.code) != -1)
  && (goodResult.indexOf(getCodeSaltyAndSweet.code) != -1) && (goodResult.indexOf(getCodePlateContain.code) != -1)
  && (goodResult.indexOf(getCodeSweetenDrinks.code) != -1))){
    returningBlocks.set_attributes["good.healthy.message"] = "true";
  } else{
    returningBlocks.set_attributes["good.healthy.message"] = "false";
  }

  if (expectingResults.indexOf(getCodeCupsize.code) != -1) {
    returningBlocks.set_attributes["eat.veg.and.fruits"] = getCodeCupsize.code;
  }

  if (expectingResults.indexOf(getCodeFriedAndFats.code) != -1){
    returningBlocks.set_attributes["moderate.fats.and.oils"] = getCodeFriedAndFats.code;
  }

  if (expectingResults.indexOf(getCodeSaltyAndSweet.code) != -1){
    returningBlocks.set_attributes["less.salt.and.sugar"] = getCodeSaltyAndSweet.code;
  }

  if (expectingResults.indexOf(getCodePlateContain.code) != -1){
    returningBlocks.set_attributes["eat.variety.foods"] = getCodePlateContain.code;
  }

  if (expectingResults.indexOf(getCodeSweetenDrinks.code) != -1){
    returningBlocks.set_attributes["drink.healthier"] = getCodeSweetenDrinks.code;
  }

  res.send(returningBlocks);
});

module.exports = routerEatingHabitScoring;