const express = require('express');
const routerChildHealth = require('express').Router();
const model = require("../model/datahandler");

var errorInputMessage = {
                          "Eng" : "You have input invalid choices. Please try again",
                          "Hil" : "Sala nga choices ang imo gin-enter. Palihog liwat sang imo sabat."
}

routerChildHealth.post('/child_health', function(req, res) {
    console.log("routerChildHealth post");
    var lang = req.body.lang.substring(0,3);
    var child_med = req.body.child_med;
    var child_illness = req.body.child_illness;
    var baby_med_proc = req.body.baby_med_proc;
    var child_block = req.body.child_block;
    var jsonResponse = {};
    
    if(typeof baby_med_proc !== "undefined") {
        if(baby_med_proc.split(",").some(isNaN) &&
           baby_med_proc.toLowerCase() != "none" &&
           baby_med_proc.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + child_block]
			};
        } else {
            jsonResponse = {};
        }
    }

    if(typeof child_med !== "undefined") {
        if(child_med.split(",").some(isNaN) &&
           child_med.toLowerCase() != "none" &&
           child_med.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + child_block]
			};
        } else {
            jsonResponse = {};
        }
    }
    
    if(typeof child_illness !== "undefined") {
        if(child_illnesssplit(",").some(isNaN) &&
           child_illness.toLowerCase() != "none" &&
           child_illness.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + child_block]
			};
        } else {
            jsonResponse = {};
        }
    }
    
    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerChildHealth.post('/save_child_health', function(req, res) {
    console.log("routerChildHealth post");   
    var jsonResponse = {};

    model.save_data("child_health", req.body);

    var jsonResponse = {
 			"messages": [
   					{"text": "Your child health info data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});


module.exports = routerChildHealth;