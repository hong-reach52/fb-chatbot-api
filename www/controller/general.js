const express = require('express');
const routerGeneral = require('express').Router();
const model = require("../model/datahandler");

routerGeneral.post('/save_gen_details', function(req, res) {
    console.log("routerGeneral post");   
    var jsonResponse = {};
    model.save_data("general", req.body);
    var jsonResponse = {
 			"messages": [
   					{"text": "Your general identity data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});

module.exports = routerGeneral;