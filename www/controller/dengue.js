exports.positive = (request, response) => {
    const { user_answer, good_block, bad_block } = request.body;
    let block = bad_block;

    if (user_answer.indexOf("1") != -1 || (user_answer.indexOf("១") != -1)) { 
        block = good_block;
    }

    return response.status(200).json({
        "redirect_to_blocks": [ block ],
    })
}