module.exports = {
    compute: function (height, weight){
        var code = "";
        var bmi = 0;
        try {
          weight = parseFloat(weight);
          height = parseFloat(height);
          if (isNaN(weight) && isNaN(height)) {
            code = "not.given"
          }
          else {
            bmi = parseFloat(weight/((height/100) * (height/100))).toFixed(1);
            if(15.9 > bmi){
                code = "sev.undw";
            }
            else if(16.0 <= bmi && 18.5 > bmi){
                code = "undw";
            }
            else if(18.5 <= bmi && 25.0 > bmi) {
                code = "nrml";
            } 
            else if(25.0 <= bmi && 30.0 > bmi) {
                code = "ovw";
            } 
            else if(30.0 <= bmi){
                code = "obse";
            } 
          }
        }
        catch(e) {
          code = "";
        }  
    
    
        return {"code" : code, "result" : bmi};
      }
}