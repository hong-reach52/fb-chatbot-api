module.exports = async (survey) => {
    let questions = [
        "1. Do you experience severe head ache frequently?\n\n1. Yes\n2. No",
        "2. Are you experiencing shortness in breath?\n\n1. Yes\n2. No",
        '3. Have you experienced sudden dizziness?\n\n1. Yes\n2. No',
        '4. Are you experiencing chest pains?\n\n1. Yes\n2. No',
        '5. Do you frequently experience sudden blurring of vision (not caused by bad sight)?\n\n1. Yes\n2. No',
        '6. Are you currently overweight or obese? (check your BMI)\n\n1. Yes\n2. No',
        '7. Do you have a close relative diagnosed with Hypertension?\n\n1. Yes\n2. No'
    ]

    let symCount = await require("../analyse")(survey, questions)

    let response = ""
    switch (symCount) {
        case 0:
            response = "Based on your answers, you are not presenting any symptoms relating to Hypertension nor presenting any risk factor for it. However, hypertension is a silent killer - most people do not know they have it. The most accurate way is to get a blood pressure check and consult your doctor. You can book a Blood Pressure check by through this service by pressing 2. "
            break;
        case 1:
            response = "Based on your answers, you are not presenting any symptoms relating to Hypertension, but you do present a risk factor/s and this should be a concern as it increases your chances of developing the disease. I highly advise you check your blood pressure regularly and consult with a doctor to get on top of your health. You can book a Blood Pressure check by through this service by pressing 2. "
            break;
        case 2:
            response = "Based on your answers, you are presenting some symptoms relating to Hypertension. Although a few, this should still be a concern, check your blood pressure and consult a doctor to get full diagnosis. You can book a Blood Pressure check by through this service by pressing 2. "
            break;
        case 3:
            response = "Based on your answers, you are presenting some symptoms relating to Hypertension. Although a few, you are also present a risk factor/s which increases the possibility of developing the disease. Get on top of it by checking your blood pressure and consulting a doctor to get full diagnosis. You can book a Blood Pressure check by through this service by pressing 2."
            break;
        case 4:
            response = "Based on your answers, you are presenting symptoms relating to Hypertension. I highly advise you to check your blood pressure and to get a consultation from your doctor to get full diagnosis or book a Blood Pressure check by through this service by pressing 2. "
            break;
        case 5:
        case 6:
        case 7:
            response = "Based on your answers, you are presenting symptoms relating to Hypertension and risk factor/s that increases the possibility of developing the disease. I highly advise you to check your blood pressure and to get a consultation from your doctor to get full diagnosis or book a Blood Pressure check by through this service by pressing 2"
            break;
        default:
            response = "Based on your answers, you are not presenting any symptoms relating to Hypertension nor presenting any risk factor for it. However, hypertension is a silent killer - most people do not know they have it. The most accurate way is to get a blood pressure check and consult your doctor. You can book a Blood Pressure check by through this service by pressing 2."
            break;
    }

    return response
}